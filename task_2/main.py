from PyQt6.QtCore import Qt, QThread, QObject, pyqtSignal
from PyQt6.QtWidgets import *

from scapy.all import *
from scapy.layers.l2 import ARP


class Worker(QObject):
    finished = pyqtSignal()
    progress = pyqtSignal(str, str)

    def run(self):
        def arp_display(pkt):
            if pkt[ARP].op == 1:  # who-has (request)
                print(f"Request: {pkt[ARP].psrc} is asking about {pkt[ARP].pdst}")
            if pkt[ARP].op == 2:  # is-at (response)
                self.progress.emit(pkt[ARP].psrc, pkt[ARP].hwsrc)
                print(f"*Response: {pkt[ARP].hwsrc} has address {pkt[ARP].psrc}")

        conf.use_pcap = True
        sniff(prn=arp_display, filter="arp", store=0)

        self.finished.emit()


class MainWindow(QMainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        self.thread = None
        self.worker = None
        self.layout = QVBoxLayout()

        self.setWindowTitle('Arp table')
        self.setFixedSize(300, 600)

        self.button = QPushButton('Show arp table')
        self.button.setFixedSize(100, 50)
        self.button.clicked.connect(self.show_arp_table)
        self.layout.addWidget(self.button, alignment=Qt.AlignmentFlag.AlignCenter)

        self.table = QTableWidget()
        self.table.setColumnCount(2)
        self.table.setHorizontalHeaderLabels(['ip address', 'mac address'])
        self.table.setColumnWidth(0, 135)
        self.table.setColumnWidth(1, 135)

        self.table.setAutoScroll(True)
        self.layout.addWidget(self.table)

        widget = QWidget()
        widget.setLayout(self.layout)

        self.setCentralWidget(widget)

    def show_arp_table(self):
        self.thread = QThread()
        self.worker = Worker()

        self.worker.moveToThread(self.thread)

        self.thread.started.connect(self.worker.run)
        self.worker.finished.connect(self.thread.quit)
        self.worker.finished.connect(self.worker.deleteLater)
        self.thread.finished.connect(self.thread.deleteLater)
        self.worker.progress.connect(self.reportProgress)

        self.thread.start()

        self.button.setEnabled(False)
        self.thread.finished.connect(
            lambda: self.button.setEnabled(True)
        )

    def reportProgress(self, ip, mac):
        print('ip', ip,  'mac', mac)
        self.table.insertRow(self.table.rowCount())
        self.table.setItem(self.table.rowCount() - 1, 0, QTableWidgetItem(str(ip)))
        self.table.setItem(self.table.rowCount() - 1, 1, QTableWidgetItem(str(mac)))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    app.exec()
