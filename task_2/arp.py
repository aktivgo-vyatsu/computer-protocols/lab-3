from scapy.all import *
from scapy.layers.l2 import *\


def scan_ips(interface='en0', ips='10.168.255.255/16') -> list[str]:
    try:
        print('[*] Start to scan')
        conf.verb = 0
        ether = Ether(dst="ff:ff:ff:ff:ff:ff")
        arp = ARP(pdst=ips)
        answer, unanswered = srp(ether / arp, timeout=1, iface=interface, inter=0)

        result = []
        for _, received in answer:
            result.append(received.summary())
        return result

    except KeyboardInterrupt:
        print('[*] User requested Shutdown')
        print('[*] Quitting...')
        sys.exit(1)

scan_ips()