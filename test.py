# https://thepacketgeek.com/scapy/building-network-tools/part-05/
# https://thepacketgeek.com/scapy/sniffing-custom-actions/part-1/

from scapy.all import *
from scapy.layers.l2 import ARP


def arp_display(pkt):
    if ARP not in pkt:
        return
    # if pkt[ARP].op == 1:  # who-has (request)
    #     return f"Request: {pkt[ARP].psrc} is asking about {pkt[ARP].pdst}"
    if pkt[ARP].op == 2:  # is-at (response)
        return f"*Response: {pkt[ARP].hwsrc} has address {pkt[ARP].psrc}"


conf.use_pcap = True
sniff(prn=arp_display, filter="arp", store=0, count=1000)
