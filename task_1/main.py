from icmplib.sockets import ICMPv4Socket
from icmplib.models import ICMPRequest
from icmplib.utils import unique_identifier
from icmplib.exceptions import ICMPLibError
from time import sleep

if __name__ == '__main__':
    address = input('input address: ')
    count = int(input('input count: '))
    print()

    id = unique_identifier()
    reply_count = 0
    timeout = 2

    with ICMPv4Socket() as sock:
        for sequence in range(count):
            if sequence > 0:
                sleep(1)

            request = ICMPRequest(
                destination=address,
                id=id,
                sequence=sequence
            )

            try:
                sock.send(request)

                reply = sock.receive(request, timeout)
                reply.raise_for_status()
                reply_count += 1

                rtt = (reply.time - request.time) * 1000

                print('%d bytes from %s: icmp_seq=%d ttl=%d time=%f ms' %
                      (reply.bytes_received, address, reply.sequence, timeout, rtt))

            except ICMPLibError:
                pass

        loss = float(((count - reply_count) / count) * 100)
        print('\n%d packets transmitted, %d packets received, %.2f%% packet loss' %
              (count, reply_count, loss))
